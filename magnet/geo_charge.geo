// Gmsh project created on Fri Jul 19 19:31:22 2019
SetFactory("OpenCASCADE");
//+
Circle(1) = {0, 0, 0, 60, 0, Pi/2};
//+
Circle(2) = {0, 0, 0, 45, 0, 25*Pi/180};
//+
Circle(3) = {0, 0, 0, 55, 0, 25*Pi/180};
//+
Circle(4) = {0, 0, 0, 55, 30*Pi/180, 50*Pi/180};
//+
Circle(5) = {0, 0, 0, 45, 30*Pi/180, 50*Pi/180};
//+
Circle(6) = {0, 0, 0, 45, 60*Pi/180, 70*Pi/180};
//+
Circle(7) = {0, 0, 0, 55, 60*Pi/180, 70*Pi/180};
//+
Circle(8) = {0, 0, 0, 55, 80*Pi/180, 84*Pi/180};
//+
Circle(9) = {0, 0, 0, 45, 80*Pi/180, 84*Pi/180};
//+
Line(10) = {15, 17};
//+
Line(11) = {18, 16};
//+
Line(12) = {13, 11};
//+
Line(13) = {12, 14};
//+
Line(14) = {7, 9};
//+
Line(15) = {10, 8};
//+
Line(16) = {5, 3};
//+
Line(17) = {6, 4};
//+
Point(19) = {0, 0, 0, 1.0};
//+
Line(18) = {1, 19};
//+
Line(19) = {19, 2};
//+
Curve Loop(1) = {18, 19, -1};
//+
Plane Surface(1) = {1};//+
Curve Loop(2) = {9, 11, -8, 10};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {6, 13, -7, 12};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {5, 15, -4, 14};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {2, -17, -3, 16};
//+
Plane Surface(5) = {5};
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; Surface{3}; Surface{4}; Surface{5}; }
//+
Physical Surface("wire", 2) = {2, 3, 4, 5};
//+
Physical Surface("background", 1) = {1};
//+
Physical Curve("neumann", 3) = {20, 17, 19};
//+
Physical Curve("dirichlet", 4) = {21};
//+
Physical Curve("boundary", 5) = {22};
//+
Transfinite Curve {21, 20} = 20 Using Progression 1;
//+
Transfinite Curve {2, 3, 5, 4} = 15 Using Progression 1;
//+
Transfinite Curve {6, 7} = 10 Using Progression 1;
//+
Transfinite Curve {9, 8} = 4 Using Progression 1;
//+
Transfinite Curve {10, 11, 12, 13, 14, 15, 16} = 5 Using Progression 1;
//+
Circle(22) = {0, 0, 0, 80, 0, Pi/2};
//+
Line(23) = {22, 19};
//+
Line(24) = {20, 23};
//+
Curve Loop(6) = {18, 24, -22, 23};
//+
Plane Surface(6) = {6};
//+
Physical Surface("iron", 6) = {6};
//+
Transfinite Curve {18, 22} = 30 Using Progression 1;
//+
Transfinite Surface {2} = {17, 18, 16, 15};
//+
Transfinite Surface {3} = {11, 12, 14, 13};
//+
Transfinite Surface {4} = {9, 10, 8, 7};//+
Transfinite Surface {6} = {22, 19, 20, 23};
//+
Transfinite Curve {24, 23} = 4 Using Progression 1;
//+
Transfinite Curve {17} = 15 Using Progression 1;
