SetFactory("OpenCASCADE");
//+
Circle(1) = {0, 0, 0, 60, 0, Pi/2};
//+
Circle(2) = {0, 0, 0, 45, 0, 25*Pi/180};
//+
Circle(3) = {0, 0, 0, 55, 0, 25*Pi/180};
//+
Circle(4) = {0, 0, 0, 55, 30*Pi/180, 50*Pi/180};
//+
Circle(5) = {0, 0, 0, 45, 30*Pi/180, 50*Pi/180};
//+
Circle(6) = {0, 0, 0, 45, 60*Pi/180, 70*Pi/180};
//+
Circle(7) = {0, 0, 0, 55, 60*Pi/180, 70*Pi/180};
//+
Circle(8) = {0, 0, 0, 55, 80*Pi/180, 84*Pi/180};
//+
Circle(9) = {0, 0, 0, 45, 80*Pi/180, 84*Pi/180};
//+
Line(10) = {15, 17};
//+
Line(11) = {18, 16};
//+
Line(12) = {13, 11};
//+
Line(13) = {12, 14};
//+
Line(14) = {7, 9};
//+
Line(15) = {10, 8};
//+
Line(16) = {5, 3};
//+
Line(17) = {6, 4};
//+
Point(19) = {0, 0, 0, 1.0};
//+
Line(18) = {1, 19};//+
Line(19) = {19, 2};
//+
Curve Loop(1) = {18, 19, -1};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {2, -17, -3, 16};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {5, 15, -4, 14};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {6, 13, -7, 12};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {9, 11, -8, 10};
//+
Plane Surface(5) = {5};
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{5}; Surface{4}; Surface{3}; Surface{2}; }
//+
Circle(22) = {0, 0, 0, 80, 0, Pi/2};
//+
Line(23) = {19, 22};
//+
Line(24) = {20, 23};
//+
Curve Loop(6) = {23, 22, -24, -18};
//+
Plane Surface(6) = {6};
//+
Physical Surface("iron", 6) = {6};
//+
Physical Surface("wire", 2) = {5, 4, 3, 2};
//+
Physical Surface("background", 1) = {1};
//+
Physical Curve("neumann", 3) = {20, 17, 19, 24};
//+
Physical Curve("dirichlet", 4) = {21, 23};
//+
Physical Curve("boundary", 5) = {22};
//+
Transfinite Curve {9, 8} = 3 Using Progression 1;
//+
Transfinite Curve {6, 7} = 8 Using Progression 1;
//+
Transfinite Curve {12, 13} = 8 Using Progression 1;
//+
Transfinite Curve {11, 10} = 8 Using Progression 1;
//+
Transfinite Curve {14, 15, 16, 17} = 8 Using Progression 1;
//+
Transfinite Curve {5, 4} = 12 Using Progression 1;
//+
Transfinite Curve {2, 3} = 14 Using Progression 1;
//+
Transfinite Curve {21} = 40 Using Progression 1;
//+
Transfinite Curve {20} = 30 Using Progression 1;
//+
Transfinite Curve {19} = 3 Using Progression 1;
//+
Transfinite Curve {18} = 50 Using Progression 1;
//+
Transfinite Curve {22} = 50 Using Progression 1;
//+
Transfinite Curve {23, 24} = 12 Using Progression 1;
//+
Transfinite Surface {3} = {7, 9, 10, 8};
//+
Transfinite Surface {5} = {15, 17, 18, 16};
//+
Transfinite Surface {4} = {13, 11, 12, 14};
//+
Transfinite Surface {2} = {3, 4, 6, 5};
//+
Transfinite Surface {6} = {22, 19, 20, 23};